variable "prefix" {
  default     = "raad"
  type        = string
  description = "the prefix for this project"
}

variable "project" {
  default     = "recipe-app-api-devops"
  type        = string
  description = "(optional) describe your variable"
}

variable "contact" {
  default     = "baerdata.ger@googlemail.com"
  type        = string
  description = "(optional) describe your variable"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  type        = string
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "terraform-tutorial"
}


variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "145868664741.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "145868664741.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
